package com.example.images;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    ImageView imageView;
    Button btnclcok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnclcok = findViewById(R.id.btncau2);
        btnclcok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,Clock.class);
                startActivity(intent);
            }
        });


    }
    public void rotereImage(View view){
imageView = findViewById(R.id.image1);
        Animation animation  = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotare);
        imageView.startAnimation(animation);
    }

    public void rotereImage2(View view) {
        imageView = findViewById(R.id.image1);
        Animation animation  = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotare2);
        imageView.startAnimation(animation);
    }

    public void rotereImage3(View view) {
        imageView = findViewById(R.id.image1);
        Animation animation  = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotare3);
        imageView.startAnimation(animation);
    }

    public void rotereImage4(View view) {
        imageView = findViewById(R.id.image1);
        imageView.setImageResource(R.drawable.all);
        Animation animation  = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotare4);
        imageView.startAnimation(animation);
    }

    public void Nobita(View view) {
        imageView = findViewById(R.id.image1);
        imageView.setImageResource(R.drawable.nobita);
        Animation animation  = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotare4);
        imageView.startAnimation(animation);
    }

    public void Doraemon(View view) {
        imageView = findViewById(R.id.image1);
        imageView.setImageResource(R.drawable.doraemon);
        Animation animation  = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotare4);
        imageView.startAnimation(animation);
    }
}
