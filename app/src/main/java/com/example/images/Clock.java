package com.example.images;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class Clock extends AppCompatActivity {
    ImageView imageView2,imageView3,imageView4,imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clock);

        imageView2 = findViewById(R.id.imageView2);
        imageView3 = findViewById(R.id.imageView3);
        imageView4 = findViewById(R.id.imageView4);
        starClock();
    }
    public void starClock(){
        Animation animationhour = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.hour);
        Animation animationminute = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.minute);
        Animation animationsecond = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.second);
        imageView2.startAnimation(animationhour);
        imageView3.startAnimation(animationminute);
        imageView4.startAnimation(animationsecond);
    }
}
